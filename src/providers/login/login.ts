import { Http , Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginProvider {

  constructor(public http: Http) {
    console.log('Hello LoginProvider Provider');
  }

  getLogin(mail:string, pass:string){
    return new Promise((resolve,reject)=>{
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({headers:headers});
      let body = {mail:mail,pass:pass};
      this.http.post('http://durandin.abrilintranet.cl/login', body, options)
      .map(res=>res.json())
      .subscribe(data => {
        resolve(data);
      },error =>{
        reject(error);
      });
    });
  }

}

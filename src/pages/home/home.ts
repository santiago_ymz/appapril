import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { AlertController } from 'ionic-angular';
import * as $ from "jquery";
import { StatusBar } from '@ionic-native/status-bar';
// import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  url: string;
  email: any;
  pass: any;

  constructor(
    private iab: InAppBrowser, 
    public navCtrl: NavController, 
    private storage: Storage, 
    public http: HttpClient, 
    public alertCtrl: AlertController,
    private statusBar: StatusBar) {
    // this.statusBar.backgroundColorByHexString('#395799');
    this.statusBar.hide();
  }

  showAlert(title, subtitle, label) {
    if (label === 'null') {
      label = null;
    } else {
      label = [label]
    }
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: label
    });
    alert.present();
  }

  saveDataUser(email, password) {
    this.storage.set('email', email);
    this.storage.set('password', password);
    this.validData(email, password);
  }  
  validData(email, password) {
    let self: any = this;
    $.ajax({
      url: 'http://durandin.abrilintranet.cl/login',
      data: { "data[User][email]": email, "data[User][password]": password },
      type: 'post',
      // beforeSend: function () {
      //   if (self.getData) {
      //     self.showAlert('Iniciando Sesión','Validando datos de usuario','ok');
      //   }
      // },
      success: function (data) {
        let resultadoPregunta = data.indexOf('Usuario o contraseña no válidos');
        if (resultadoPregunta == -1) {
          self.getData = true;
          // console.log('redirigido');
          self.openWebPage('http://durandin.abrilintranet.cl/login/', self.email, self.pass);
        } else {
          // console.log('error');
          self.showAlert('Usuario no Válido', 'Email o Password no válidos', 'Volver a intentar');
        }
      }
    });
  }

  loadDataUser() {
    Promise.all([this.storage.get("email"), this.storage.get("password")]).then(values => {
      this.email = values[0];
      this.pass = values[1];
      // console.log(this.email, this.pass);
      if (this.email == null || this.pass == null) {
        // console.log('Datos guardados son nulos');
        return null;
      } else {
        // console.log('existe un registro');
        this.validData(this.email, this.pass);
      }
    });
  }
  

  // antes de que se cargue el modulo
  ionViewDidLoad() {
    this.loadDataUser()
  }

  clearDataUser(email, password) {
    this.storage.clear();
    this.email = null;
    this.pass = null;
  } 


  // Carga la página
  openWebPage(url:string, email:string, pass:string) {
    // console.log('openWebPage',this.email, this.pass);
    const pageContent: string = '<html><head></head><body><form id="loginForm" action="' + url +'" method="post"><input type="hidden" name="data[User][email]" id="UserEmail" value="' + email + '"><input type="hidden" name="data[User][password]" id="UserPassword" value="' + pass + '"></form> <script type="text/javascript">document.getElementById("loginForm").submit();</script></body></html>';
    const pageContentUrl = 'data:text/html;base64,' + btoa(pageContent);    
    const options: InAppBrowserOptions = {
      zoom: 'no',
      location: 'no',
      clearcache: 'yes',
      toolbar: 'no'
    }
    const browser = this.iab.create(pageContentUrl, '_blank', options);

    browser.on('loadstop').subscribe(function (event) {
      browser.executeScript({ code: '' });
    });

  }
  goToPassword(){
    this.iab.create('http://durandin.abrilintranet.cl/recuperar-clave', '_blank');
  }
}
